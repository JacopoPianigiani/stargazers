//
//  InputViewController.swift
//  Stargazers
//
//  Created by Jacopo Pianigiani on 30/12/21.
//

import UIKit

class InputViewController: UIViewController {
    @IBOutlet weak var repositoryNameView:TextFieldValidationView!
    @IBOutlet weak var repositoryOwnerView:TextFieldValidationView!
    @IBOutlet weak var showStargazersButton:UIButton!
    
    fileprivate var repositoryName:String?
    fileprivate var repositoryOwner:String?
    
    //MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        repositoryNameView.titleLabel.text = "Repository Name"
        repositoryNameView.textField.placeholder = "Insert repository name"
        repositoryOwnerView.titleLabel.text = "Repository Owner"
        repositoryOwnerView.textField.placeholder = "Insert repository owner"
        repositoryNameView.delegate = self
        repositoryOwnerView.delegate = self
        
        showStargazersButton.layer.cornerRadius = showStargazersButton.frame.height/2
        showStargazersButton.isEnabled = false
        
        //AccessibilityIdenitifers for UI tests
        repositoryNameView.textField.accessibilityIdentifier = "NameTextField"
        repositoryNameView.errorMessageLabel.accessibilityIdentifier = "NameErrorMessage"
        repositoryOwnerView.textField.accessibilityIdentifier = "OwnerTextField"
        repositoryOwnerView.errorMessageLabel.accessibilityIdentifier = "OwnerErrorMessage"
        showStargazersButton.accessibilityIdentifier = "ShowButton"
    }
        
    //MARK: - Button action
    
    @IBAction func didSelectShowButton(_ sender:UIButton){
        performSegue(withIdentifier: "showStargazersList", sender: nil)
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationViewController = segue.destination as? ListViewController {
            destinationViewController.repositoryName = repositoryName
            destinationViewController.repositoryOwner = repositoryOwner
        }
    }
}

extension InputViewController:TextFieldValidationViewDelegate{
    func editingCompleted(view: TextFieldValidationView) {
        var otherView = repositoryOwnerView
        
        if view == repositoryNameView {
            repositoryOwnerView.textField.becomeFirstResponder() //Move focus on text field of other view
            repositoryName = repositoryNameView.textField.text?.lowercased().trimmingCharacters(in: .whitespaces)
        }
        else {
            otherView = repositoryNameView
            repositoryOwner = repositoryOwnerView.textField.text?.lowercased().trimmingCharacters(in: .whitespaces)
        }
    
        let viewIsValid = view.isValid
        let otherViewIsValid = otherView?.isValid ?? false
        showStargazersButton.isEnabled = viewIsValid && otherViewIsValid ? true : false
    }
}
