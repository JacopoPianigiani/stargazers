//
//  String+Extension.swift
//  Stargazers
//
//  Created by Jacopo Pianigiani on 30/12/21.
//

import Foundation

extension String {
    var containsSpecialCharacters:Bool {
        let expressionRange = range(of: "[!\"#$%&'()*+,./:;<=>?@\\[\\\\\\]^_`{|}~]+", options: .regularExpression)
        return expressionRange != nil
    }
}
