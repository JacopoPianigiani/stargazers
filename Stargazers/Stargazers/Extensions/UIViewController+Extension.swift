//
//  UIViewController+Extension.swift
//  Stargazers
//
//  Created by Jacopo Pianigiani on 30/12/21.
//

import Foundation
import UIKit

extension UIViewController {
    func displayAlertControllerWithPopAction(messageDescription:String){
        let alertController = UIAlertController(title: "Warning", message: messageDescription, preferredStyle: .alert)
        let popAction = UIAlertAction(title: "Dismiss", style: .cancel, handler: {
            [unowned self]
            action in
            self.navigationController?.popViewController(animated: true)
        })
        
        alertController.addAction(popAction)
        alertController.view.accessibilityIdentifier = "alertController"
        
        DispatchQueue.main.async {
            [weak self] in
            guard let self = self else { return }
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
}
