//
//  ListPresenter.swift
//  Stargazers
//
//  Created by Jacopo Pianigiani on 30/12/21.
//

import Foundation
import UIKit

protocol ListPresenterDelegate:AnyObject {
    func showStargazers(stagazersListViewModel:StargazersListViewModel)
    func displayErrorMessage(message:String)
}

class ListPresenter {
    weak var delegate:ListPresenterDelegate?
    private var networkHelper:NetworkProtocol
    
    init(helper:NetworkProtocol){
        self.networkHelper = helper
    }
    
    //MARK: - Interface methods
    
    func getStargazers(repositoryName:String, repositoryOwner:String, page:Int){
        networkHelper.getStargazers(repositoryName: repositoryName, repositoryOwner: repositoryOwner, page: String(page), completion: {
            [weak self]
            list in
            
            guard let self = self else { return }
            let viewModel = StargazersListViewModel(stargazersList: list)
            self.delegate?.showStargazers(stagazersListViewModel: viewModel)
            
        }, errorCompletion: {
            [weak self]
            errorMessage in
            
            guard let self = self else { return }
            self.delegate?.displayErrorMessage(message: errorMessage)
        })
    }
    
    func getStargazerAvatar(url:String, adaptToSize:CGSize, completion:@escaping(UIImage)->Void, errorCompletion:@escaping()->Void){
        networkHelper.getStargazerAvatar(url: url, completion: {
            image in
            
            let imageRatio = adaptToSize.width/image.size.width
            if let imageData = image.pngData(),
               let scaledImage = UIImage(data: imageData, scale: imageRatio){
                
                completion(scaledImage)
            }
        }, errorCompletion: {
            errorCompletion()
        })
    }
}
