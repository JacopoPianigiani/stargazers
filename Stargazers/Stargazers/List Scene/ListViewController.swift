//
//  ListViewController.swift
//  Stargazers
//
//  Created by Jacopo Pianigiani on 30/12/21.
//

import UIKit

class ListViewController: UIViewController {
    @IBOutlet weak var spinner:UIActivityIndicatorView!
    @IBOutlet weak var tableView:UITableView!
    
    fileprivate var presenter:ListPresenter?
    
    var repositoryName:String?
    var repositoryOwner:String?
    fileprivate var currentPage = 0
    fileprivate var isLoading = false
    
    fileprivate lazy var viewModels:StargazersListViewModel = {
       return StargazersListViewModel(stargazersList: [])
    }()
    
    //MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        
        presenter = ListPresenter(helper: NetworkHelper())
        presenter?.delegate = self
        
        requestPage()
    }
    
    fileprivate func buildDataSource(){
        DispatchQueue.main.async {
            [weak self] in
            guard let viewModelsList = self?.viewModels,
                      viewModelsList.numberOfItems() > 0 else {
                          return
                      }
            
            self?.tableView.isHidden = false
            self?.tableView.reloadData()
            self?.isLoading = false
        }
    }
    
    fileprivate func configureTableView(){
        tableView.register(UINib(nibName: StargazerTableViewCellIdentifier, bundle: nil), forCellReuseIdentifier: StargazerTableViewCellIdentifier)
        tableView.register(UINib(nibName: loadingTableViewCellIdentifier, bundle: nil), forCellReuseIdentifier: loadingTableViewCellIdentifier)
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.estimatedRowHeight = 130.0
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    //MARK: - Page request
    
    fileprivate func requestPage(){
        guard !isLoading else {
            return
        }
        
        if let name = repositoryName,
           let owner = repositoryOwner {
            
            isLoading = true
            currentPage += 1
            
            presenter?.getStargazers(repositoryName: name, repositoryOwner: owner, page: currentPage)
        }
    }
}

extension ListViewController:UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            return 1 //Loading cell
        }
        
        return self.viewModels.numberOfItems()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: StargazerTableViewCellIdentifier, for: indexPath)
            
            if let stargazerCell = cell as? StargazerTableViewCell {
                let viewModel = viewModels.getItem(index: indexPath.row)
                
                stargazerCell.stargazerUsernameLabel.text = viewModel?.stargazerUsername ?? ""
            
                if let avatarUrl = viewModel?.avatarURL,
                   !avatarUrl.isEmpty {
                    
                    presenter?.getStargazerAvatar(url: avatarUrl, adaptToSize: stargazerCell.imageView?.frame.size ?? CGSize.zero, completion: {
                        image in
                        stargazerCell.spinner.stopAnimating()
                        stargazerCell.stargazerImageView.image = image
                    }, errorCompletion: {
                        stargazerCell.spinner.stopAnimating()
                    })
                }
            }
            
            return cell
                        
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: loadingTableViewCellIdentifier, for: indexPath)
            
            if let loadingCell = cell as? LoadingTableViewCell {
                loadingCell.spinner.startAnimating()
            }
            
            return cell
        }
    }
}

extension ListViewController:UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return UITableView.automaticDimension
            
        default:
            return 70
        }
    }
}

extension ListViewController:UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        if currentPage >= 1,
           offsetY > contentHeight - scrollView.frame.height {
            requestPage()
        }
    }
}

extension ListViewController:ListPresenterDelegate {
    func showStargazers(stagazersListViewModel: StargazersListViewModel) {
        viewModels.addItems(viewModels: stagazersListViewModel.getItems())
        buildDataSource()
    }
    
    func displayErrorMessage(message: String) {
        displayAlertControllerWithPopAction(messageDescription: message)
        isLoading = false
    }
}
