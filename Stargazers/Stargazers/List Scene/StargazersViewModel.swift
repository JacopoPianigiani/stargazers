//
//  StargazersViewModel.swift
//  Stargazers
//
//  Created by Jacopo Pianigiani on 30/12/21.
//

import Foundation

struct StargazersListViewModel {
    private var list:[StargazerViewModel]
    
    init(stargazersList:StargazersList){
        self.list = stargazersList.compactMap({
            guard let url = $0.avatarURL,
                  let username = $0.login else {
                      return nil
                  }
            
            return StargazerViewModel(avatarURL: url, stargazerUsername: username)
        })
    }
    
    //MARK: - Get methods
    
    func getItems()->[StargazerViewModel]{
        return list
    }
    
    func getItem(index:Int)->StargazerViewModel? {
        guard index < list.count else {
            return nil
        }
        
        return list[index]
    }
    
    func numberOfItems()->Int {
        return list.count
    }
    
    //MARK: - Modify methods
    
    mutating func addItems(viewModels:[StargazerViewModel]){
        list.append(contentsOf: viewModels)
    }
    
    mutating func removeAll(){
        list.removeAll()
    }
}

struct StargazerViewModel {
    let avatarURL:String?
    let stargazerUsername:String?
}
