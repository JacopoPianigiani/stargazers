//
//  LoadingTableViewCell.swift
//  BeersList
//
//  Created by Jacopo Pianigiani on 07/09/21.
//

import UIKit

let loadingTableViewCellIdentifier = "LoadingTableViewCell"

class LoadingTableViewCell: UITableViewCell {
    @IBOutlet weak var spinner:UIActivityIndicatorView!
}
