//
//  StargazerTableViewCell.swift
//  Stargazers
//
//  Created by Jacopo Pianigiani on 30/12/21.
//

import UIKit

let StargazerTableViewCellIdentifier = "StargazerTableViewCell"

class StargazerTableViewCell: UITableViewCell {
    @IBOutlet weak var spinner:UIActivityIndicatorView!
    @IBOutlet weak var stargazerImageView:UIImageView!
    @IBOutlet weak var stargazerUsernameLabel:UILabel!

    override func prepareForReuse() {
        super.prepareForReuse()
        stargazerImageView.image = nil
        stargazerUsernameLabel.text = ""
        spinner.startAnimating()
    }
}
