//
//  TextFieldValidationView.swift
//  Stargazers
//
//  Created by Jacopo Pianigiani on 30/12/21.
//

import UIKit

protocol TextFieldValidationViewDelegate:AnyObject{
    func editingCompleted(view:TextFieldValidationView)
}

class TextFieldValidationView: UIView {
    @IBOutlet weak var contentView:UIView!
    @IBOutlet weak var textField:UITextField!
    @IBOutlet weak var titleLabel:UILabel!
    @IBOutlet weak var errorMessageLabel:UILabel!
    
    var isValid = false
    weak var delegate:TextFieldValidationViewDelegate?
    
    //MARK: - Setup UI
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        contentView = loadViewFromNib()
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        addSubview(contentView)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "TextFieldValidationView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        errorMessageLabel.isHidden = true
        textField.delegate = self
    }
    
    //MARK: - Validation
    
    @discardableResult
    fileprivate func validate()->Bool{
        guard let textInserted = textField.text,
              !textInserted.isEmpty else {
                  showErrorMessage(message: "You must to insert something!")
                  return false
              }
        
        if textInserted.containsSpecialCharacters {
            showErrorMessage(message: "Special characters not allowed")
            return false
        }
        
        errorMessageLabel.isHidden = true
        isValid = true
        textField.layer.borderColor = UIColor.green.cgColor
        textField.layer.borderWidth = 2.0
        textField.layer.cornerRadius = 6
        return true
    }
    
    fileprivate func showErrorMessage(message:String){
        isValid = false
        errorMessageLabel.text = message
        errorMessageLabel.isHidden = false
        textField.layer.borderWidth = 2.0
        textField.layer.borderColor = UIColor.red.cgColor
        textField.layer.cornerRadius = 6
    }
}

extension TextFieldValidationView:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        validate()
        delegate?.editingCompleted(view: self)
    }
}
