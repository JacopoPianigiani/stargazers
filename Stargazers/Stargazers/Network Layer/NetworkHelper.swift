//
//  NetworkHelper.swift
//  Stargazers
//
//  Created by Jacopo Pianigiani on 30/12/21.
//

import Foundation
import UIKit

let githubAPIUrl = "https://api.github.com/repos"
let decodingFailureMessage = "Failure while decoding response data"

protocol NetworkProtocol {
    func getStargazers(repositoryName:String, repositoryOwner:String, page:String, completion:@escaping(StargazersList)->Void, errorCompletion:@escaping(String)->Void)
    
    func getStargazerAvatar(url:String, completion:@escaping(UIImage)->Void, errorCompletion:@escaping()->Void)
}

class NetworkHelper:NetworkProtocol {
    private let imagesDownloadQueue = DispatchQueue(label: "ImagesDownloadQueue", qos: .utility, attributes: .concurrent)
    private let requestQueue = DispatchQueue(label: "SearchQueriesQueue", qos: .userInteractive, attributes: .concurrent)
    private let session:URLSession
                                             
    init(){
        //Session configuration
        let urlSessionConfiguration = URLSessionConfiguration.default
        urlSessionConfiguration.requestCachePolicy = .returnCacheDataElseLoad
        urlSessionConfiguration.timeoutIntervalForRequest = 20.0
        urlSessionConfiguration.httpMaximumConnectionsPerHost = 20

        self.session = URLSession(configuration: urlSessionConfiguration)
    }
    
    func getStargazers(repositoryName: String, repositoryOwner: String, page:String, completion: @escaping (StargazersList) -> Void, errorCompletion: @escaping (String) -> Void) {
        let requestURL = URL(string: githubAPIUrl)?.appendingPathComponent("\(repositoryOwner)").appendingPathComponent("\(repositoryName)/stargazers")
        guard let url = requestURL else {
            errorCompletion("Request URL nil")
            return
        }
        
        requestQueue.async {
            //Add query string parameters to url
            let queryParams = ["per_page": "20", "page": page]
            
            var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: true)
            urlComponents?.queryItems = queryParams.map({
                URLQueryItem(name: $0.key, value: $0.value)
            })
            
            guard let reqUrl = urlComponents?.url else {
                errorCompletion("Request URL nil")
                return
            }
            
            var request = URLRequest(url: reqUrl)
            request.httpMethod = "GET"
            request.setValue("application/vnd.github.v3+json", forHTTPHeaderField: "Accept")
            
            let task = self.session.dataTask(with: request, completionHandler: {
                (data, response, responseError) in
                
                DispatchQueue.main.async {
                    guard responseError == nil else {
                        errorCompletion("Get request failed: \(responseError!.localizedDescription )")
                        return
                    }
                    
                    if let responseData = data {
                        if let list = ResponseValidator.validate(type: StargazersList.self, fromResponseData: responseData) {
                            completion(list)
                            return
                        }
                        
                        var errorMessage = decodingFailureMessage
                        if let errorModel = ResponseValidator.validate(type: ErrorModel.self, fromResponseData: responseData),
                           let message = errorModel.message {
                            errorMessage = message
                        }
                        
                        errorCompletion(errorMessage)
                    }
                }
            })
            
            task.resume()
        }
    }
    
    func getStargazerAvatar(url: String, completion: @escaping (UIImage) -> Void, errorCompletion: @escaping () -> Void) {
        guard let requestURL = URL(string: url) else {
            errorCompletion()
            return
        }
        
        imagesDownloadQueue.async {
            let task = self.session.dataTask(with: requestURL, completionHandler: {
                (data, response, responseError) in
                
                DispatchQueue.main.async {
                    guard responseError == nil else {
                        errorCompletion()
                        return
                    }
                    
                    if let responseData = data,
                       let image = UIImage(data: responseData){
                        completion(image)
                    }
                }
            })
            
            task.resume()
        }
    }
}
