//
//  ErrorModel.swift
//  Stargazers
//
//  Created by Jacopo Pianigiani on 30/12/21.
//

import Foundation

struct ErrorModel: Codable {
    let message: String?
    let documentationURL: String?

    enum CodingKeys: String, CodingKey {
        case message
        case documentationURL = "documentation_url"
    }
}
