//
//  ResponseValidator.swift.swift
//  Stargazers
//
//  Created by Jacopo Pianigiani on 30/12/21.
//

import Foundation

struct ResponseValidator {
    static func validate<T: Codable>(type: T.Type, fromResponseData response:Data) -> T? {
        do {
            let decoded = try JSONDecoder().decode(T.self, from: response)
            return decoded
        } catch {
            return nil
        }
    }
}



