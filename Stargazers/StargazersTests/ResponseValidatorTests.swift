//
//  ResponseValidatorTests.swift
//  StargazersTests
//
//  Created by Jacopo Pianigiani on 31/12/21.
//

import XCTest
@testable import Stargazers

class ResponseValidatorTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    //MARK: - Validate tests
    
    func testValidator_validateJsonStargazersListResponse_positive()  {
        guard let stargazersListData = getdataFromBundle(resourceName: requestResponseJSON, resourceType: ".json") else {
            XCTFail("Needed resource not available")
            return
        }
        
        let decodedObject = ResponseValidator.validate(type: StargazersList.self, fromResponseData: stargazersListData)
        XCTAssert(decodedObject != nil)
    }
    
    func testValidator_validateJsonErrorResponse_positive()  {
        guard let errorResponseData = getdataFromBundle(resourceName: errorResponseJSON, resourceType: ".json") else {
            XCTFail("Needed resource not available")
            return
        }
        
        let decodedObject = ResponseValidator.validate(type: ErrorModel.self, fromResponseData: errorResponseData)
        XCTAssert(decodedObject != nil)
    }
}
