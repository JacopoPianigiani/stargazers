//
//  StargazersViewModelTest.swift
//  StargazersTests
//
//  Created by Jacopo Pianigiani on 31/12/21.
//

import XCTest
@testable import Stargazers

class StargazersViewModelTest: XCTestCase {
    var viewModels:StargazersListViewModel?
    
    override func setUpWithError() throws {
        if let stargazersList = getObjectFromJsonFileInBundle(jsonFileName: requestResponseJSON, decodingType: StargazersList.self) {
            viewModels = StargazersListViewModel(stargazersList: stargazersList)
        }
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    //MARK: - Get methods tests
    
    func testStargazersViewModel_getItem_postive()  {
        let itemsNumber = viewModels?.numberOfItems() ?? 0
        guard itemsNumber > 0 else {
            XCTFail("View models empty or nil")
            return
        }
        
        let item = viewModels?.getItem(index: 0)
        XCTAssert(item != nil)
    }
    
    func testStargazersViewModel_getItem_negative()  {
        let itemsNumber = viewModels?.numberOfItems() ?? 0
        guard itemsNumber > 0 else {
            XCTFail("View models empty or nil")
            return
        }
        
        let item = viewModels?.getItem(index: itemsNumber)
        XCTAssert(item == nil)
    }
    
    func testStargazersViewModel_getItems_positive(){
        let itemsNumber = viewModels?.numberOfItems() ?? 0
        guard itemsNumber > 0 else {
            XCTFail("View models empty or nil")
            return
        }
        
        let items = viewModels?.getItems() ?? []
        XCTAssert(!items.isEmpty)
    }
    
    //MARK: - Modify methods tests
    
    func testStargazersViewModel_addItem_positive(){
        let newItemViewModel = StargazerViewModel(avatarURL: "https://avatars.githubusercontent.com/u/412564?v=4", stargazerUsername: "slycrel")
        let numberOfItems = viewModels?.numberOfItems() ?? 0
        viewModels?.addItems(viewModels: [newItemViewModel])
        let newNumberOfItems = viewModels?.numberOfItems() ?? 0
        XCTAssert(newNumberOfItems == numberOfItems + 1)
    }
    
    func testStargazersViewModel_removeAllItems_positive(){
        viewModels?.removeAll()
        let numberOfItems = viewModels?.numberOfItems() ?? 0
        XCTAssert(numberOfItems == 0)
    }
}
