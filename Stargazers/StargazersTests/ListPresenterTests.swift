//
//  ListPresenterTests.swift
//  StargazersTests
//
//  Created by Jacopo Pianigiani on 31/12/21.
//

import XCTest
@testable import Stargazers

class ListPresenterTests: XCTestCase {
    var presenter:ListPresenter?
    
    override func setUpWithError() throws {}

    override func tearDownWithError() throws {
        presenter = nil
    }

    //MARK: - Ger stargazers tests
    
    func testListPresenter_getStargazers_positive() {
        let networkHelperMocked = NetworkHelperMocked()
        presenter = ListPresenter(helper: networkHelperMocked) //Dependency injection
        
        let delegateObject = PresenterDelegateObject()
        delegateObject.testExpectation =  XCTestExpectation(description: "Waiting delegate show method invoked")

        presenter?.delegate = delegateObject
        presenter?.getStargazers(repositoryName: "hello-world", repositoryOwner: "octocat", page: 1)

        wait(for: [delegateObject.testExpectation!], timeout: 10.0)
        
        XCTAssert(delegateObject.showMethodInvoked)
    }
    
    func testListPresenter_getStargazers_negative() {
        let networkHelperMocked = NetworkHelperMockedWithFailure()
        presenter = ListPresenter(helper: networkHelperMocked) //Dependency injection
        
        let delegateObject = PresenterDelegateObject()
        delegateObject.testExpectation = XCTestExpectation(description: "Waiting delegate display error method invoked")

        presenter?.delegate = delegateObject
        presenter?.getStargazers(repositoryName: "hello-world", repositoryOwner: "octocat", page: 1)

        wait(for: [delegateObject.testExpectation!], timeout: 10.0)
        
        XCTAssert(delegateObject.displayErroMethodInvoked)
    }
    
    //MARK: - Get stargazer avatar tests
    
    func testListPresenter_getStargazerAvatar_positive() {
        let networkHelperMocked = NetworkHelperMocked()
        presenter = ListPresenter(helper: networkHelperMocked) //Dependency injection
        
        presenter?.getStargazerAvatar(url: "https://api.github.com/users/mattiasjahnke", adaptToSize: CGSize.zero, completion: {
            image in
            XCTAssert(true)
        }, errorCompletion: {
            XCTAssert(false)
        })
    }
    
    func testListPresenter_getStargazerAvatar_negative() {
        let networkHelperMocked = NetworkHelperMockedWithFailure()
        presenter = ListPresenter(helper: networkHelperMocked) //Dependency injection
        
        presenter?.getStargazerAvatar(url: "https://api.github.com/users/mattiasjahnke", adaptToSize: CGSize.zero, completion: {
            image in
            XCTAssert(false)
        }, errorCompletion: {
            XCTAssert(true)
        })
    }
}

//Custom delegate object to test methods invocation

class PresenterDelegateObject:ListPresenterDelegate {
    var showMethodInvoked = false
    var displayErroMethodInvoked = false
    var testExpectation:XCTestExpectation?
    
    func showStargazers(stagazersListViewModel: StargazersListViewModel) {
        testExpectation?.fulfill()
        showMethodInvoked = true
    }
    
    func displayErrorMessage(message: String) {
        testExpectation?.fulfill()
        displayErroMethodInvoked = true
    }
}
