//
//  NetworkHelperMocked.swift
//  StargazersTests
//
//  Created by Jacopo Pianigiani on 30/12/21.
//

import Foundation
import UIKit
@testable import Stargazers

class NetworkHelperMocked:NetworkProtocol {
    
    //MARK: - Request methods
    
    func getStargazers(repositoryName: String, repositoryOwner: String, page: String, completion: @escaping (StargazersList) -> Void, errorCompletion: @escaping (String) -> Void) {
        guard let responseJSON = getdataFromBundle(resourceName: requestResponseJSON, resourceType: "json") else {
            errorCompletion("Stargazers list response nil")
            return
        }
        
        if let list = ResponseValidator.validate(type: StargazersList.self, fromResponseData: responseJSON) {
            completion(list)
            return
        }
        
        errorCompletion(decodingFailureMessage)
    }
    
    func getStargazerAvatar(url: String, completion: @escaping (UIImage) -> Void, errorCompletion: @escaping () -> Void) {
        guard let avatarData = getdataFromBundle(resourceName: "avatar", resourceType: "png") else {
            errorCompletion()
            return
        }
        
        if let image = UIImage(data: avatarData) {
            completion(image)
            return
        }
        
        errorCompletion()
    }
    
    //MARK: - Bundle helper function
    
    func getdataFromBundle(resourceName:String, resourceType:String)->Data?{
        let bundle = Bundle(for: type(of: self))
        guard let path = bundle.path(forResource: resourceName, ofType: resourceType) else {
            return nil
        }
        
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
            return data
        } catch {
            print("Error while reading resource: \(resourceName) of type: \(resourceType)")
            return nil
        }
    }
}

//Mocked class to simulate failure cases

class NetworkHelperMockedWithFailure:NetworkHelperMocked {
    override func getStargazers(repositoryName: String, repositoryOwner: String, page: String, completion: @escaping (StargazersList) -> Void, errorCompletion: @escaping (String) -> Void) {
        errorCompletion(decodingFailureMessage)
     }
    
    override func getStargazerAvatar(url: String, completion: @escaping (UIImage) -> Void, errorCompletion: @escaping () -> Void) {
        errorCompletion()
    }
}
