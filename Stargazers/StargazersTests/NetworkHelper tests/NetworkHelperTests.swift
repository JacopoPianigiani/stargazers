//
//  NetworkHelperTests.swift
//  StargazersTests
//
//  Created by Jacopo Pianigiani on 30/12/21.
//

import XCTest
@testable import Stargazers

class NetworkHelperTests: XCTestCase {
    var networkHelper = NetworkHelperMocked()
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    //MARK: - Request methods tests
    
    func testNetworkHelper_getStargazersList_positive() {
        networkHelper.getStargazers(repositoryName: "swift",
                                    repositoryOwner: "apple",
                                    page: "1",
                                    completion: {
            list in
            XCTAssert(list.count > 0)
        }, errorCompletion: {
            errorMessage in
            XCTAssert(false)
        })
    }
    
    func testNetworkHelper_getAvatar_positive(){
        networkHelper.getStargazerAvatar(url: "https://avatars.githubusercontent.com/u/3087023?v=4", completion: {
            image in
            XCTAssert(true)
        }, errorCompletion: {
            XCTAssert(false)
        })
    }
}
