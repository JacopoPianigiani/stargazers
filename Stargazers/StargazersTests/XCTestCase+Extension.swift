//
//  XCTestCase+Extension.swift
//  StargazersTests
//
//  Created by Jacopo Pianigiani on 31/12/21.
//

import Foundation
import XCTest
@testable import Stargazers

let requestResponseJSON = "RequestResponse"
let errorResponseJSON = "ErrorResponse"

extension XCTestCase {
    
    //MARK: - Load data from app bundle
    
    func getdataFromBundle(resourceName:String, resourceType:String)->Data?{
        let bundle = Bundle(for: type(of: self))
        guard let path = bundle.path(forResource: resourceName, ofType: resourceType) else {
            return nil
        }
        
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
            return data
        } catch {
            print("Error while reading resource: \(resourceName) of type: \(resourceType)")
            return nil
        }
    }
    
    func getObjectFromJsonFileInBundle<T:Codable>(jsonFileName:String, decodingType:T.Type)->T?{
        guard let jsonData = getdataFromBundle(resourceName: jsonFileName, resourceType: "json") else {
            return nil
        }
        
        if let decodedObjet = ResponseValidator.validate(type: T.self, fromResponseData: jsonData) {
            return decodedObjet
        }
        
        return nil
    }
}
