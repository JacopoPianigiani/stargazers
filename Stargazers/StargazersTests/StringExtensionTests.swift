//
//  StringExtensionTests.swift
//  StargazersTests
//
//  Created by Jacopo Pianigiani on 30/12/21.
//

import XCTest
@testable import Stargazers

class StringExtensionTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    //MARK: - String contains special characters tests
    
    func testString_containsSpecialCharacters_positive(){
        let testString = "apple[*"
        XCTAssert(testString.containsSpecialCharacters)
    }
    
    func testString_containsSpecialCharacters_negative(){
        let testString = "apple"
        XCTAssertTrue(!testString.containsSpecialCharacters)
    }
}
