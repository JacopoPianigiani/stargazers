//
//  StargazersUITests.swift
//  StargazersUITests
//
//  Created by Jacopo Pianigiani on 30/12/21.
//

import XCTest
@testable import Stargazers

class StargazersUITests: XCTestCase {

    override func setUpWithError() throws {
        continueAfterFailure = false
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    //MARK: - Input scene tests
    
    func testStargazers_RepositoryDetailsValidation_negative() throws {
        let app = XCUIApplication()
        app.launch()
        
        let nameTextField = app.textFields["NameTextField"]
        let ownerTextField = app.textFields["OwnerTextField"]

        nameTextField.tap()
        nameTextField.typeText("Hello-World*")
        
        let returnButton = app.keyboards.buttons["Return"]
        returnButton.tap()

        ownerTextField.tap()
        ownerTextField.typeText("Octocat")
        returnButton.tap()
        
        let nameErrorMessage = app.staticTexts.element(matching: .staticText, identifier: "NameErrorMessage")
        XCTAssertTrue(nameErrorMessage.label == "Special characters not allowed")
    }
    
    //MARK: - Stargazers list navigation tests
    
    func testStargazers_navigationToStargazersList_positive() throws {
        let app = XCUIApplication()
        app.launch()
        
        let nameTextField = app.textFields["NameTextField"]
        let ownerTextField = app.textFields["OwnerTextField"]

        nameTextField.tap()
        nameTextField.typeText("Hello-World")
        
        let returnButton = app.keyboards.buttons["Return"]
        returnButton.tap()

        ownerTextField.tap()
        ownerTextField.typeText("Octocat")
        returnButton.tap()
        
        let showButton = app.buttons.element(matching: .button, identifier: "ShowButton")
        showButton.tap()
        
        let tableViewCells = app.tables.children(matching: .cell)
        let _ = tableViewCells.element.waitForExistence(timeout: 20) //Wait amount of time equal to network layer timeout
        XCTAssertTrue(tableViewCells.count > 0)
    }
    
    func testStargazers_navigationToStargazersList_negative() throws {
        let app = XCUIApplication()
        app.launch()
        
        let nameTextField = app.textFields["NameTextField"]
        let ownerTextField = app.textFields["OwnerTextField"]

        nameTextField.tap()
        nameTextField.typeText("android")
        
        let returnButton = app.keyboards.buttons["Return"]
        returnButton.tap()

        ownerTextField.tap()
        ownerTextField.typeText("Google")
        returnButton.tap()
        
        let showButton = app.buttons.element(matching: .button, identifier: "ShowButton")
        showButton.tap()
        
        //Get alert controller shown to user
        let alertController = app.alerts.element(matching: .alert, identifier: "alertController")
        let _ = alertController.waitForExistence(timeout: 20)
        let dismissButtonText = NSLocalizedString("Dismiss", comment: "")
        XCTAssertTrue(alertController.buttons.firstMatch.label == dismissButtonText)
        
        let tableViewCells = app.tables.children(matching: .cell)
        XCTAssertTrue(tableViewCells.count == 0)
    }
}
